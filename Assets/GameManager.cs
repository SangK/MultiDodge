﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
    public static GameManager instace;

	// Use this for initialization
	void Start () {
        instace = this;
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
   
    public void GameOver()
    {
        Time.timeScale = 0;
        FindObjectOfType<GameUI>().OnGameOver();
    }
}
