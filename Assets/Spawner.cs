﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {
    public int StartingBullet;
    public GameObject bulletPrefab;
    public Transform WallPrefab;
    public float RespawnTime=2f;
    public float nextRespawnTime;
    // Use this for initialization
    void Start ()
    {
        Vector2 Left = Camera.main.ViewportToWorldPoint(new Vector2(-1, -1));
        //nextRespawnTime = Time.time + RespawnTime;
        for (int i = 0; i < StartingBullet; i++)
        {
            Vector3 position = new Vector3(Random.Range(-10f, 10f), Random.Range(-10f,10f), 0f);
            if (position.x > 0)
                position.x += 10f;
            else
                position.x -= 10f;
            if (position.y > 0)
                position.y += 10f;
            else
                position.y -= 10f;
            Instantiate(bulletPrefab, position, Quaternion.identity);
               
        }
     

	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Time.time>nextRespawnTime)
        {
            Vector3 position = new Vector3(Random.Range(-10f, 10f), Random.Range(-10f, 10f), 0f);
            if (position.x > 0)
                position.x += 10f;
            else
                position.x -= 10f;
            if (position.y > 0)
                position.y += 10f;
            else
                position.y -= 10f;
            Instantiate(bulletPrefab, position, Quaternion.identity);
            nextRespawnTime = Time.time + RespawnTime;
        }
	}
}
