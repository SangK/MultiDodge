﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    public float speed;
    Rigidbody myRigidbody;
    Vector3 velocity;
    private void Start()
    {
        myRigidbody = GetComponent<Rigidbody>();
    }
   
    public void Move(Vector3 _velocity)
    {
        
        velocity = _velocity;
    }
    public void FixedUpdate()
    {
        
        myRigidbody.MovePosition(myRigidbody.position + velocity * Time.fixedDeltaTime);

       

    }

}
