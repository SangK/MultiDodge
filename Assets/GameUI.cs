﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class GameUI : MonoBehaviour {
    public Image fadePlane;
    public GameObject gameoverUI;
    public Text text;
    public Text bullettext;
    float origintime;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        origintime += Time.deltaTime;
        text.text = "Time : " + string.Format("{0:00.00}", origintime);
        int count = FindObjectsOfType<Bullet>().Length;

        bullettext.text = "Bullet : " + count.ToString();
	}
    public void OnGameOver()
    {
        StartCoroutine(Fade(Color.clear, Color.white, 1));
        gameoverUI.SetActive(true);
    }
    IEnumerator Fade(Color from, Color to, float time)
    {
        float speed = 1 / time;
        float percent = 0;

        while (percent < 1)
        {
            percent += Time.deltaTime * speed;
            fadePlane.color = Color.Lerp(from, to, percent);
            yield return null;

        }
    }
    public void ReStart()
    {
        SceneManager.LoadScene("Game");
        origintime = 0f;
        Time.timeScale = 1f;
    }
}
