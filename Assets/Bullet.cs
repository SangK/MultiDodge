﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    Vector3 dir;
    public float panBorderThickenss = 10f;
    public float speed;
    float acc;
	// Use this for initialization
	void Start () {
        dir = Vector3.zero - transform.position;
        dir = dir.normalized;
        acc = Random.Range(5f, 6f);
	}
    private void OnBecameInvisible()
    {
        dir = Vector3.zero - transform.position;
        dir = dir.normalized;
    }

    // Update is called once per frame
    void Update () {
        dir = dir.normalized;
        transform.Translate(dir*speed*Time.deltaTime*acc);
        Vector3 viewpos =Camera.main.WorldToViewportPoint(transform.position);
        if (viewpos.y > 1)
            dir.y = Random.Range(-1f, 0f);
        if (viewpos.y < 0)
            dir.y = Random.Range(0f, 1f);
        if (viewpos.x > 1)
            dir.x = Random.Range(-1f, 0f);
        if (viewpos.x < 0)
            dir.x = Random.Range(0f, 1f);

        

	}
}
