﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
    public float moveSpeed = 5f;

    PlayerController controller;
	// Use this for initialization
	void Start () {
        controller = GetComponent<PlayerController>();
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 moveInput = new Vector3(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"), 0f);
        Vector3 moveVelocity = moveInput.normalized * moveSpeed;
        controller.Move(moveVelocity);
	}
    private void OnTriggerEnter(Collider other)
    {
        GameObject.Destroy(gameObject);
        GameManager.instace.GameOver();
    }
}
